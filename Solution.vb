Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic
Imports FillRet = System.Tuple(Of Integer, Integer(,))
Imports Tp = System.Tuple(Of Integer, Integer)

Public Class KnightsAndPawns
    Dim startTime As Integer, limitTime As Integer
    Dim rand As New Random(19831983)
    Dim H, W As Integer
    Dim board(,) As Integer

    Const Pawn As Integer = 1 << 12
    Const Knight As Integer = 1 << 15
    Const KeepOut As Integer = 1 << 6

    Dim dxs() As Integer = { 1, -1,  1, -1,  2,  2, -2, -2 }
    Dim dys() As Integer = { 2,  2, -2, -2,  1, -1,  1, -1 }

    Public Function placeKnights(board_str() As String) As String()
        startTime = Environment.TickCount
        limitTime = startTime + 9400
        ' Console.Error.WriteLine("{0}, {1}", limitTime, rand.Next(0, 100))

        H = board_str.Length
        W = board_str(0).Length
        ' Console.Error.WriteLine("H: {0}, W: {1}", H, W)
        If H * W < 3000 Then
            DfsLimit *= 5
        ElseIf H * W < 6000 Then
            DfsLimit *= 2
        ElseIf H * W < 8000 Then
            DfsLimit = DfsLimit * 3 \ 2
        End If

        ReDim board(H - 1, W - 1)

        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                If board_str(i)(j) <> "."c Then
                    board(i, j) = board(i, j) Or Pawn
                    For d As Integer = 0 To UBound(dxs)
                        Dim y As Integer = i + dys(d)
                        If y < 0 OrElse y >= H Then Continue For
                        Dim x As Integer = j + dxs(d)
                        If x < 0 OrElse x >= W Then Continue For
                        board(y, x) += 1
                    Next d
                End If
            Next j
        Next i

        Dim fr As FillRet = GetSolution()
        Dim score As Integer = fr.Item1
        Dim tmp(,) As Integer = fr.Item2

        board = tmp
        Console.Error.WriteLine("sc: {0}", score)

        Dim ret(H - 1) As String
        For i As Integer = 0 To H - 1
            Dim st As String = ""
            For j As Integer = 0 To W - 1
                If board(i, j) < Knight Then
                    st += "."
                Else
                    st += "K"
                End If
            Next j
            ret(i) = st
        Next i
        placeKnights = ret

        Console.Error.WriteLine("total time: {0} ms", Environment.TickCount - startTime)
    End Function

    Private Function GetSolution() As FillRet

        Dim rBoard(,) As Integer = CType(board.Clone(), Integer(,))
        Dim rSc As Integer = FillRems(rBoard)

        Dim rrBoard(,) As Integer = CType(board.Clone(), Integer(,))
        Dim rrSc As Integer = FillRemsRand(rrBoard)

        Dim fr As FillRet = FillA(board, 5)
        Dim score As Integer = fr.Item1
        Dim tmp(,) As Integer = fr.Item2
        score += FillWIncInc(tmp)
        score += FillHIncInc(tmp)
        score += Connect(tmp)
        score += KnightLoop(tmp)
        score += FillRems(tmp)
        ' Console.Error.WriteLine("sc: {0}", score)

        For u As Integer = 0 To 9
        For b As Integer = 0 To 1
        For f As Integer = 0 To 2
        For r As Integer = 2 To 0 Step -1
        For k As Integer = 0 To 4
        For j As Integer = 0 To 3
        For i As Integer = 1 To 5
            Dim timeX As Integer = Environment.TickCount
            If timeX > limitTime Then
                GetSolution = New FillRet(score, tmp)
                Exit Function
            End If
            Dim scoreZ = 0
            If r = 0 Then
                If b = 0 Then
                    Select Case j
                    Case 0
                        fr = FillA(rBoard, i)
                    Case 1
                        fr = FillB(rBoard, i)
                    Case 2
                        fr = FillC(rBoard, i)
                    Case 3
                        fr = FillD(rBoard, i)
                    End Select
                    scoreZ = rSc
                Else
                    Select Case j
                    Case 0
                        fr = FillA(rrBoard, i)
                    Case 1
                        fr = FillB(rrBoard, i)
                    Case 2
                        fr = FillC(rrBoard, i)
                    Case 3
                        fr = FillD(rrBoard, i)
                    End Select
                    scoreZ = rrSc
                End If
            Else
                Select Case j
                Case 0
                    fr = FillA(board, i)
                Case 1
                    fr = FillB(board, i)
                Case 2
                    fr = FillC(board, i)
                Case 3
                    fr = FillD(board, i)
                End Select
            End If
            scoreZ += fr.Item1
            Dim tmpZ(,) As Integer = fr.Item2
            If r = 1 Then
                If b = 0 Then
                    scoreZ += FillRems(tmpZ)
                Else
                    scoreZ += FillRemsRand(tmpZ)
                End If
            End If
            Select Case u
            Case 0
                Select Case k
                Case 0
                    scoreZ += FillWIncInc(tmpZ)
                    scoreZ += FillHIncInc(tmpZ)
                Case 1
                    scoreZ += FillHIncInc(tmpZ)
                    scoreZ += FillWIncInc(tmpZ)
                Case 2
                    scoreZ += FillWIncInc(tmpZ)
                Case 3
                    scoreZ += FillHIncInc(tmpZ)
                End Select
            Case 1
                Select Case k
                Case 0
                    scoreZ += FillWDecDec(tmpZ)
                    scoreZ += FillHDecDec(tmpZ)
                Case 1
                    scoreZ += FillHDecDec(tmpZ)
                    scoreZ += FillWDecDec(tmpZ)
                Case 2
                    scoreZ += FillWDecDec(tmpZ)
                Case 3
                    scoreZ += FillHDecDec(tmpZ)
                End Select
            Case 2
                Select Case k
                Case 0
                    scoreZ += FillWIncDec(tmpZ)
                    scoreZ += FillHIncDec(tmpZ)
                Case 1
                    scoreZ += FillHIncDec(tmpZ)
                    scoreZ += FillWIncDec(tmpZ)
                Case 2
                    scoreZ += FillWIncDec(tmpZ)
                Case 3
                    scoreZ += FillHIncDec(tmpZ)
                End Select
            Case 3
                Select Case k
                Case 0
                    scoreZ += FillWDecInc(tmpZ)
                    scoreZ += FillHDecInc(tmpZ)
                Case 1
                    scoreZ += FillHDecInc(tmpZ)
                    scoreZ += FillWDecInc(tmpZ)
                Case 2
                    scoreZ += FillWDecInc(tmpZ)
                Case 3
                    scoreZ += FillHDecInc(tmpZ)
                End Select
            Case 4
                Select Case k
                Case 0
                    scoreZ += FillWIncInc(tmpZ)
                    scoreZ += FillHDecDec(tmpZ)
                Case 1
                    scoreZ += FillHDecDec(tmpZ)
                    scoreZ += FillWIncInc(tmpZ)
                Case 2
                    scoreZ += FillWDecInc(tmpZ)
                    scoreZ += FillHIncDec(tmpZ)
                Case 3
                    scoreZ += FillHIncDec(tmpZ)
                    scoreZ += FillWDecInc(tmpZ)
                End Select
            Case 5
                Select Case k
                Case 0
                    scoreZ += FillWDecDec(tmpZ)
                    scoreZ += FillHIncInc(tmpZ)
                Case 1
                    scoreZ += FillHIncInc(tmpZ)
                    scoreZ += FillWDecDec(tmpZ)
                Case 2
                    scoreZ += FillWIncDec(tmpZ)
                    scoreZ += FillHDecInc(tmpZ)
                Case 3
                    scoreZ += FillHDecInc(tmpZ)
                    scoreZ += FillWIncDec(tmpZ)
                End Select
            Case 6
                Select Case k
                Case 0
                    scoreZ += FillWDecDec(tmpZ)
                    scoreZ += FillHIncDec(tmpZ)
                Case 1
                    scoreZ += FillHIncDec(tmpZ)
                    scoreZ += FillWDecDec(tmpZ)
                Case 2
                    scoreZ += FillWDecDec(tmpZ)
                    scoreZ += FillHDecInc(tmpZ)
                Case 3
                    scoreZ += FillHDecInc(tmpZ)
                    scoreZ += FillWDecDec(tmpZ)
                End Select
            Case 7
                Select Case k
                Case 0
                    scoreZ += FillWIncInc(tmpZ)
                    scoreZ += FillHDecInc(tmpZ)
                Case 1
                    scoreZ += FillHDecInc(tmpZ)
                    scoreZ += FillWIncInc(tmpZ)
                Case 2
                    scoreZ += FillWIncInc(tmpZ)
                    scoreZ += FillHIncDec(tmpZ)
                Case 3
                    scoreZ += FillHIncDec(tmpZ)
                    scoreZ += FillWIncInc(tmpZ)
                End Select
            Case 8
                Select Case k
                Case 0
                    scoreZ += FillWDecInc(tmpZ)
                    scoreZ += FillHIncInc(tmpZ)
                Case 1
                    scoreZ += FillHIncInc(tmpZ)
                    scoreZ += FillWDecInc(tmpZ)
                Case 2
                    scoreZ += FillWIncDec(tmpZ)
                    scoreZ += FillHIncInc(tmpZ)
                Case 3
                    scoreZ += FillHIncInc(tmpZ)
                    scoreZ += FillWIncDec(tmpZ)
                End Select
            Case 9
                Select Case k
                Case 0
                    scoreZ += FillWDecInc(tmpZ)
                    scoreZ += FillHDecDec(tmpZ)
                Case 1
                    scoreZ += FillHDecDec(tmpZ)
                    scoreZ += FillWDecInc(tmpZ)
                Case 2
                    scoreZ += FillWIncDec(tmpZ)
                    scoreZ += FillHDecDec(tmpZ)
                Case 3
                    scoreZ += FillHDecDec(tmpZ)
                    scoreZ += FillWIncDec(tmpZ)
                End Select
            End Select
            Select Case f
            Case 0
                scoreZ += Connect(tmpZ)
                scoreZ += KnightLoop(tmpZ)
            Case 1
                scoreZ += ConnectOld(tmpZ)
                scoreZ += KnightLoopOld(tmpZ)
            Case 2
                scoreZ += ConnectRand(tmpZ)
                scoreZ += KnightLoopRand(tmpZ)
            End Select
            If r >= 2 Then
                If b = 0 Then
                    scoreZ += FillRems(tmpZ)
                Else
                    scoreZ += FillRemsRand(tmpZ)
                End If
            End If
            If scoreZ > score Then
                score = scoreZ
                tmp = CType(tmpZ.Clone(), Integer(,))
                ' CopyArray2D(tmpZ, tmp)
                ' Console.Error.WriteLine("update: u:{5} b:{6} f:{4} r:{0} k:{1} j:{2} i:{3}", r, k, j, i, f, u, b)
            End If
        Next i
        Next j
        Next k
        Next r
        Next f
        Next b
        Next u

        GetSolution = New FillRet(score, tmp)
    End Function

    Private Function PutKnight(tmp(,) As Integer, y As Integer, x As Integer) As Integer
        PutKnight = 0
        If tmp(y, x) >= Pawn Then Exit Function
        tmp(y, x) = tmp(y, x) Or Knight
        For d As Integer = 0 To UBound(dxs)
            Dim ty As Integer = y + dys(d)
            If ty < 0 OrElse ty >= H Then Continue For
            Dim tx As Integer = x + dxs(d)
            If tx < 0 OrElse tx >= W Then Continue For
            tmp(ty, tx) += KeepOut
        Next d
        PutKnight = 1
    End Function

    Private Function RemoveKnight(tmp(,) As Integer, y As Integer, x As Integer) As Integer
        RemoveKnight = 0
        If tmp(y, x) < Knight Then Exit Function
        tmp(y, x) = tmp(y, x) Xor Knight
        For d As Integer = 0 To UBound(dxs)
            Dim ty As Integer = y + dys(d)
            If ty < 0 OrElse ty >= H Then Continue For
            Dim tx As Integer = x + dxs(d)
            If tx < 0 OrElse tx >= W Then Continue For
            tmp(ty, tx) -= KeepOut
        Next d
        RemoveKnight = 1
    End Function

    Private Function IsNoEmpty(tmp(,) As Integer, y As Integer, x As Integer) As Boolean
        If y < 0 OrElse x < 0 OrElse y >= H OrElse x >= W Then
            IsNoEmpty = False
        Else
            IsNoEmpty = tmp(y, x) >= Pawn
        End If
    End Function

    Private Function CheckFirstA(tmp(,) As Integer, y As Integer, x As Integer) As Boolean
        CheckFirstA = False
        If y < 0 OrElse x < 0 OrElse y + 3 >= H OrElse x >= W Then Exit Function
        If tmp(y, x) >= Knight Then Exit Function
        If IsNoEmpty(tmp, y - 1, x - 2) Then Exit Function
        If IsNoEmpty(tmp, y - 1, x + 2) Then Exit Function
        If IsNoEmpty(tmp, y + 1, x - 2) Then Exit Function
        If IsNoEmpty(tmp, y - 2, x - 1) Then Exit Function
        If IsNoEmpty(tmp, y - 2, x + 1) Then Exit Function
        If IsNoEmpty(tmp, y + 2, x - 1) Then Exit Function
        If IsNoEmpty(tmp, y + 1, x    ) Then Exit Function
        If IsNoEmpty(tmp, y - 1, x    ) Then Exit Function
        If IsNoEmpty(tmp, y    , x - 1) Then Exit Function
        If IsNoEmpty(tmp, y + 1, x - 1) Then Exit Function
        y += 3
        If tmp(y, x) >= Knight Then Exit Function
        If IsNoEmpty(tmp, y - 1, x - 2) Then Exit Function
        If IsNoEmpty(tmp, y + 1, x + 2) Then Exit Function
        If IsNoEmpty(tmp, y + 1, x - 2) Then Exit Function
        If IsNoEmpty(tmp, y - 2, x - 1) Then Exit Function
        If IsNoEmpty(tmp, y + 2, x + 1) Then Exit Function
        If IsNoEmpty(tmp, y + 2, x - 1) Then Exit Function
        If IsNoEmpty(tmp, y + 1, x    ) Then Exit Function
        If IsNoEmpty(tmp, y - 1, x    ) Then Exit Function
        If IsNoEmpty(tmp, y    , x - 1) Then Exit Function
        If IsNoEmpty(tmp, y - 1, x - 1) Then Exit Function
        CheckFirstA = True
    End Function

    Private Function CheckLastA(tmp(,) As Integer, y As Integer, x As Integer) As Boolean
        CheckLastA = False
        If y < 0 OrElse x < 0 OrElse y + 3 >= H OrElse x >= W Then Exit Function
        If tmp(y, x) >= Knight Then Exit Function
        If IsNoEmpty(tmp, y - 1, x - 2) Then Exit Function
        If IsNoEmpty(tmp, y - 1, x + 2) Then Exit Function
        If IsNoEmpty(tmp, y + 1, x + 2) Then Exit Function
        If IsNoEmpty(tmp, y - 2, x - 1) Then Exit Function
        If IsNoEmpty(tmp, y - 2, x + 1) Then Exit Function
        If IsNoEmpty(tmp, y + 2, x + 1) Then Exit Function
        If IsNoEmpty(tmp, y + 1, x    ) Then Exit Function
        If IsNoEmpty(tmp, y - 1, x    ) Then Exit Function
        If IsNoEmpty(tmp, y    , x + 1) Then Exit Function
        If IsNoEmpty(tmp, y + 1, x + 1) Then Exit Function
        y += 3
        If tmp(y, x) >= Knight Then Exit Function
        If IsNoEmpty(tmp, y + 1, x + 2) Then Exit Function
        If IsNoEmpty(tmp, y + 1, x - 2) Then Exit Function
        If IsNoEmpty(tmp, y - 1, x + 2) Then Exit Function
        If IsNoEmpty(tmp, y + 2, x + 1) Then Exit Function
        If IsNoEmpty(tmp, y + 2, x - 1) Then Exit Function
        If IsNoEmpty(tmp, y - 2, x + 1) Then Exit Function
        If IsNoEmpty(tmp, y + 1, x    ) Then Exit Function
        If IsNoEmpty(tmp, y - 1, x    ) Then Exit Function
        If IsNoEmpty(tmp, y    , x + 1) Then Exit Function
        If IsNoEmpty(tmp, y - 1, x + 1) Then Exit Function
        CheckLastA = True
    End Function

    Private Function CheckBandA(tmp(,) As Integer, y As Integer, x As Integer) As Boolean
        CheckBandA = False
        If y + 1 < 0 OrElse x < 0 OrElse y + 2 >= H OrElse x >= W Then Exit Function
        If tmp(y + 1, x) >= Knight Then Exit Function
        If tmp(y + 2, x) >= Knight Then Exit Function
        If IsNoEmpty(tmp, y - 1, x) Then Exit Function
        If IsNoEmpty(tmp, y    , x) Then Exit Function
        If IsNoEmpty(tmp, y + 3, x) Then Exit Function
        If IsNoEmpty(tmp, y + 4, x) Then Exit Function
        CheckBandA = True
    End Function

    Private Function FillA(bd(,) As Integer, up As Integer) As FillRet
        Dim tmp(,) As Integer = CType(bd.Clone(), Integer(,))
        Dim cnt As Integer = 0
        Dim flag(H - 1) As Boolean
        Do
            Dim changes = 0
            Dim y As Integer = 0
            Do While y < H - 3
                If flag(y) Then
                    y += 1
                    Continue Do
                End If
                flag(y) = True
                Dim x As Integer = 0
                Dim p As Integer = 0
                Do While x < W - 3
                    If Not CheckFirstA(tmp, y, x) Then
                        x += 1
                        Continue Do
                    End If
                    Dim x0 As Integer = x
                    x += 1
                    If Not CheckBandA(tmp, y, x) Then
                        x += 1
                        Continue Do
                    End If
                    Dim x1 As Integer = -1
                    x += 1
                    Do While x < W - 1
                        If Not CheckBandA(tmp, y, x) Then
                            Exit Do
                        End If
                        x += 1
                        If Not CheckLastA(tmp, y, x) Then
                            Continue Do
                        End If
                        x1 = x
                    Loop
                    If x1 < 0 Then Continue Do
                    cnt += PutKnight(tmp, y, x0)
                    cnt += PutKnight(tmp, y + 3, x0)
                    cnt += PutKnight(tmp, y, x1)
                    cnt += PutKnight(tmp, y + 3, x1)
                    For tx As Integer = x0 + 1 To x1 - 1
                        cnt += PutKnight(tmp, y + 1, tx)
                        cnt += PutKnight(tmp, y + 2, tx)
                    Next tx
                    changes += 1
                    p += 1
                Loop
                If p > 0 Then y += up
            Loop
            If changes = 0 Then Exit Do
        Loop
        FillA = New FillRet(cnt, tmp)
    End Function

    Dim DfsLimit As Integer = 3000

    Dim dfsList As New List(Of Integer())()
    Dim dfsStack As New Stack(Of Integer)()

    Const DFSShiftor As Integer = 8
    Const DFSMask As Integer = (1 << DFSShiftor) - 1

    Private Function P2I(y As Integer, x As Integer) As Integer
        P2I = (y << DFSShiftor) Or x
    End Function

    Private Sub I2P(i As Integer, ByRef y As Integer, ByRef x As Integer)
        y = i >> DFSShiftor
        x = i And DFSMask
    End Sub

    Private Function GetLongest() As Integer()
        If dfsList.Count = 0 Then
            Dim emp(-1) As Integer
            GetLongest = emp
            Exit Function
        End If
        Dim e As Integer = 0
        For i As Integer = 1 To dfsList.Count - 1
            If dfsList(i).Length > dfsList(e).Length Then
                e = i
            End If
        Next i
        GetLongest = dfsList(e)
    End Function

    Private Function ApplyLongest(tmp(,) As Integer) As Integer
        Dim cnt As Integer = 0
        Dim ps() As Integer = GetLongest()
        For Each p As Integer In ps
            Dim ty, tx As Integer
            I2P(p, ty, tx)
            cnt += PutKnight(tmp, ty, tx)
        Next p
        ApplyLongest = cnt
    End Function

    Private Function DfsEx(tmp(,) As Integer, y As Integer, x As Integer, count As Integer) As Integer
        dfsStack.Clear()
        dfsList.Clear()
        DfsCall(tmp, y, x, count)
        DfsEx = dfsList.Count
    End Function

    Private Sub DfsCall(tmp(,) As Integer, y As Integer, x As Integer, ByRef count As Integer)
        count += 1
        If count > DfsLimit Then Exit Sub
        dfsStack.Push(P2I(y, x))
        PutKnight(tmp, y, x)
        For d As Integer = 0 To UBound(dxs)
            Dim ty As Integer = y + dys(d)
            If ty < 0 OrElse ty >= H Then Continue For
            Dim tx As Integer = x + dxs(d)
            If tx < 0 OrElse tx >= W Then Continue For
            Dim p As Integer = tmp(ty, tx) - KeepOut
            If p = 0 Then
                DfsCall(tmp, ty, tx, count)
            ElseIf p = 1 Then
                dfsStack.Push(P2I(ty, tx))
                Dim es() As Integer = dfsStack.ToArray()
                dfsList.Add(es)
                dfsStack.Pop()
            End If
        Next d
        dfsStack.Pop()
        RemoveKnight(tmp, y, x)
    End Sub

    Private Function Connect(tmp(,) As Integer) As Integer
        Dim cnt As Integer = 0
        For y As Integer = 0 To H - 1
            For x As Integer = 0 To W - 1
                If tmp(y, x) <> 1 Then Continue For
                If DfsEx(tmp, y, x, 0) = 0 Then Continue For
                cnt += ApplyLongest(tmp)
            Next x
        Next y
        Connect = cnt
    End Function

    ' Private Sub CopyArray2D(src(,) As Integer, dst(,) As Integer)
        ' For y As Integer = 0 To H - 1
            ' For x As Integer = 0 To W - 1
                ' dst(y, x) = src(y, x)
            ' Next x
        ' Next y
    ' End Sub

    Private Function FillRems(ByRef tmp(,) As Integer) As Integer
        Dim cnt As Integer = 0
        Dim emp As New Queue(Of Tp)()
        Dim flag(H - 1, W - 1) As Tp
        For y As Integer = 0 To H - 1
            For x As Integer = 0 To W - 1
                If tmp(y, x) <> 2 Then Continue For
                Dim t As New Tp(y, x)
                emp.Enqueue(t)
                flag(y, x) = t
            Next x
        Next y
        Dim ec As Integer = emp.Count
        For i As Integer = 1 To ec
            Dim t As Tp = emp.Dequeue()
            If tmp(t.Item1, t.Item2) = 2 Then
                cnt += PutKnight(tmp, t.Item1, t.Item2)
            Else
                emp.Enqueue(t)
            End If
        Next i
        If emp.Count = 0 Then
            FillRems = cnt
            Exit Function
        End If
        ' Console.Error.WriteLine("ec {0}", ec)
        Dim tmpZ(,) As Integer = CType(tmp.Clone(), Integer(,))
        Dim cntZ As Integer = cnt
        For i As Integer = 1 To ec * 5
            If emp.Count = 0 Then Exit For
            Dim t As Tp = emp.Dequeue()
            Dim y As Integer = t.Item1
            Dim x As Integer = t.Item2
            If tmpZ(y, x) = 2 Then
                cntZ += PutKnight(tmpZ, y, x)
                If cntZ > cnt Then
                    cnt = cntZ
                    tmp = CType(tmpZ.Clone(), Integer(,))
                    ' CopyArray2D(tmpZ, tmp)
                    ' Console.Error.WriteLine("upd1")
                End If
                Continue For
            End If
            Dim nc As Boolean = True
            For d As Integer = 0 To UBound(dxs)
                Dim ty As Integer = y + dys(d)
                If ty < 0 OrElse ty >= H Then Continue For
                Dim tx As Integer = x + dxs(d)
                If tx < 0 OrElse tx >= W Then Continue For
                If flag(ty, tx) Is Nothing Then Continue For
                If (tmpZ(ty, tx) And Knight) = 0 Then Continue For
                nc = False
                cntZ -= RemoveKnight(tmpZ, ty, tx)
                emp.Enqueue(flag(ty, tx))
            Next d
            If nc Then Continue For
            If tmpZ(y, x) <> 2 Then Continue For
            cntZ += PutKnight(tmpZ, y, x)
            If cntZ > cnt Then
                cnt = cntZ
                tmp = CType(tmpZ.Clone(), Integer(,))
                ' CopyArray2D(tmpZ, tmp)
                ' Console.Error.WriteLine("upd2")
            End If
        Next i
        FillRems = cnt
    End Function

    Private Function NoZeroAndNoPawn(e As Integer) As Boolean
        NoZeroAndNoPawn = e <> 0 AndAlso (e And Pawn) = 0
    End Function

    Private Function FillWIncInc(tmp(,) As Integer) As Integer
        Dim cnt As Integer = 0
        For y As Integer = 0 To H - 3
            For x As Integer = 0 To W - 5
                If NoZeroAndNoPawn(tmp(y + 1, x    )) Then Continue For
                If NoZeroAndNoPawn(tmp(y    , x + 2)) Then Continue For
                If NoZeroAndNoPawn(tmp(y + 2, x + 2)) Then Continue For
                If NoZeroAndNoPawn(tmp(y + 1, x + 4)) Then Continue For
                cnt += PutKnight(tmp, y + 1, x    )
                cnt += PutKnight(tmp, y    , x + 2)
                cnt += PutKnight(tmp, y + 2, x + 2)
                cnt += PutKnight(tmp, y + 1, x + 4)
            Next x
        Next y
        FillWIncInc = cnt
    End Function

    Private Function FillHIncInc(tmp(,) As Integer) As Integer
        Dim cnt As Integer = 0
        For x As Integer = 0 To W - 3
            For y As Integer = 0 To H - 5
                If NoZeroAndNoPawn(tmp(y    , x + 1)) Then Continue For
                If NoZeroAndNoPawn(tmp(y + 2, x    )) Then Continue For
                If NoZeroAndNoPawn(tmp(y + 2, x + 2)) Then Continue For
                If NoZeroAndNoPawn(tmp(y + 4, x + 1)) Then Continue For
                cnt += PutKnight(tmp, y    , x + 1)
                cnt += PutKnight(tmp, y + 2, x    )
                cnt += PutKnight(tmp, y + 2, x + 2)
                cnt += PutKnight(tmp, y + 4, x + 1)
            Next y
        Next x
        FillHIncInc = cnt
    End Function

    Const Mask As Integer = Pawn >> 1

    Private Function KnightLoop(tmp(,) As Integer) As Integer
        Dim cnt As Integer = 0
        For y As Integer = 0 To H - 1
            For x As Integer = 0 To W - 1
                If tmp(y, x) = 1 Then
                    tmp(y, x) = tmp(y, x) Or Mask
                End If
            Next x
        Next y
        For y As Integer = 0 To H - 1
            For x As Integer = 0 To W - 1
                If tmp(y, x) <> 0 Then Continue For
                Dim c As Boolean = True
                For d As Integer = 0 To UBound(dxs)
                    Dim ty As Integer = y + dys(d)
                    If ty < 0 OrElse ty >= H Then Continue For
                    Dim tx As Integer = x + dxs(d)
                    If tx < 0 OrElse tx >= W Then Continue For
                    If tmp(ty, tx) <> 0 Then Continue For
                    tmp(ty, tx) = 1
                    c = False
                Next d
                If c Then Continue For
                tmp(y, x) = tmp(y, x) Or Knight
                For d As Integer = 0 To UBound(dxs)
                    Dim ty As Integer = y + dys(d)
                    If ty < 0 OrElse ty >= H Then Continue For
                    Dim tx As Integer = x + dxs(d)
                    If tx < 0 OrElse tx >= W Then Continue For
                    If tmp(ty, tx) <> 1 Then Continue For
                    Dim ret = DfsEx(tmp, ty, tx, 0)
                    If ret > 0 Then
                        cnt += ApplyLongest(tmp)
                        c = True
                        Exit For
                    End If
                Next d
                For d As Integer = 0 To UBound(dxs)
                    Dim ty As Integer = y + dys(d)
                    If ty < 0 OrElse ty >= H Then Continue For
                    Dim tx As Integer = x + dxs(d)
                    If tx < 0 OrElse tx >= W Then Continue For
                    If tmp(ty, tx) <> 1 Then Continue For
                    tmp(ty, tx) = 0
                Next d
                tmp(y, x) = tmp(y, x) Xor Knight
                If c Then
                    cnt += PutKnight(tmp, y, x)
                End If
            Next x
        Next y
        ' For y As Integer = 0 To H - 1
            ' For x As Integer = 0 To W - 1
                ' If (tmp(y, x) And Mask) <> 0 Then
                    ' tmp(y, x) = tmp(y, x) Xor Mask
                ' End If
            ' Next x
        ' Next y
        KnightLoop = cnt
    End Function


    Private Function FillB(bd(,) As Integer, down As Integer) As FillRet
        Dim tmp(,) As Integer = CType(bd.Clone(), Integer(,))
        Dim cnt As Integer = 0
        Dim flag(H - 1) As Boolean
        Do
            Dim changes = 0
            Dim y As Integer = H - 4
            Do While y >= 0
                If flag(y) Then
                    y -= 1
                    Continue Do
                End If
                flag(y) = True
                Dim x As Integer = 0
                Dim p As Integer = 0
                Do While x < W - 3
                    If Not CheckFirstA(tmp, y, x) Then
                        x += 1
                        Continue Do
                    End If
                    Dim x0 As Integer = x
                    x += 1
                    If Not CheckBandA(tmp, y, x) Then
                        x += 1
                        Continue Do
                    End If
                    Dim x1 As Integer = -1
                    x += 1
                    Do While x < W - 1
                        If Not CheckBandA(tmp, y, x) Then
                            Exit Do
                        End If
                        x += 1
                        If Not CheckLastA(tmp, y, x) Then
                            Continue Do
                        End If
                        x1 = x
                    Loop
                    If x1 < 0 Then Continue Do
                    cnt += PutKnight(tmp, y, x0)
                    cnt += PutKnight(tmp, y + 3, x0)
                    cnt += PutKnight(tmp, y, x1)
                    cnt += PutKnight(tmp, y + 3, x1)
                    For tx As Integer = x0 + 1 To x1 - 1
                        cnt += PutKnight(tmp, y + 1, tx)
                        cnt += PutKnight(tmp, y + 2, tx)
                    Next tx
                    changes += 1
                    p += 1
                Loop
                If p > 0 Then y -= down
            Loop
            If changes = 0 Then Exit Do
        Loop
        FillB = New FillRet(cnt, tmp)
    End Function

        Private Function CheckFirstC(tmp(,) As Integer, y As Integer, x As Integer) As Boolean
        CheckFirstC = False
        If y < 0 OrElse x < 0 OrElse y >= H OrElse x + 3 >= W Then Exit Function
        If tmp(y, x) >= Knight Then Exit Function
        If IsNoEmpty(tmp, y - 1, x - 2) Then Exit Function
        If IsNoEmpty(tmp, y - 1, x + 2) Then Exit Function
        If IsNoEmpty(tmp, y + 1, x - 2) Then Exit Function
        If IsNoEmpty(tmp, y - 2, x - 1) Then Exit Function
        If IsNoEmpty(tmp, y - 2, x + 1) Then Exit Function
        If IsNoEmpty(tmp, y + 2, x - 1) Then Exit Function
        If IsNoEmpty(tmp, y    , x + 1) Then Exit Function
        If IsNoEmpty(tmp, y - 1, x    ) Then Exit Function
        If IsNoEmpty(tmp, y    , x - 1) Then Exit Function
        If IsNoEmpty(tmp, y - 1, x + 1) Then Exit Function
        x += 3
        If tmp(y, x) >= Knight Then Exit Function
        If IsNoEmpty(tmp, y - 1, x - 2) Then Exit Function
        If IsNoEmpty(tmp, y + 1, x + 2) Then Exit Function
        If IsNoEmpty(tmp, y - 1, x + 2) Then Exit Function
        If IsNoEmpty(tmp, y - 2, x - 1) Then Exit Function
        If IsNoEmpty(tmp, y + 2, x + 1) Then Exit Function
        If IsNoEmpty(tmp, y - 2, x + 1) Then Exit Function
        If IsNoEmpty(tmp, y    , x + 1) Then Exit Function
        If IsNoEmpty(tmp, y - 1, x    ) Then Exit Function
        If IsNoEmpty(tmp, y    , x - 1) Then Exit Function
        If IsNoEmpty(tmp, y - 1, x - 1) Then Exit Function
        CheckFirstC = True
    End Function

    Private Function CheckLastC(tmp(,) As Integer, y As Integer, x As Integer) As Boolean
        CheckLastC = False
        If y < 0 OrElse x < 0 OrElse y >= H OrElse x + 3 >= W Then Exit Function
        If tmp(y, x) >= Knight Then Exit Function
        If IsNoEmpty(tmp, y - 1, x - 2) Then Exit Function
        If IsNoEmpty(tmp, y + 1, x - 2) Then Exit Function
        If IsNoEmpty(tmp, y + 1, x + 2) Then Exit Function
        If IsNoEmpty(tmp, y - 2, x - 1) Then Exit Function
        If IsNoEmpty(tmp, y + 2, x - 1) Then Exit Function
        If IsNoEmpty(tmp, y + 2, x + 1) Then Exit Function
        If IsNoEmpty(tmp, y + 1, x    ) Then Exit Function
        If IsNoEmpty(tmp, y    , x - 1) Then Exit Function
        If IsNoEmpty(tmp, y    , x + 1) Then Exit Function
        If IsNoEmpty(tmp, y + 1, x + 1) Then Exit Function
        x += 3
        If tmp(y, x) >= Knight Then Exit Function
        If IsNoEmpty(tmp, y + 1, x + 2) Then Exit Function
        If IsNoEmpty(tmp, y + 1, x - 2) Then Exit Function
        If IsNoEmpty(tmp, y - 1, x + 2) Then Exit Function
        If IsNoEmpty(tmp, y + 2, x + 1) Then Exit Function
        If IsNoEmpty(tmp, y + 2, x - 1) Then Exit Function
        If IsNoEmpty(tmp, y - 2, x + 1) Then Exit Function
        If IsNoEmpty(tmp, y + 1, x    ) Then Exit Function
        If IsNoEmpty(tmp, y    , x - 1) Then Exit Function
        If IsNoEmpty(tmp, y    , x + 1) Then Exit Function
        If IsNoEmpty(tmp, y + 1, x - 1) Then Exit Function
        CheckLastC = True
    End Function

    Private Function CheckBandC(tmp(,) As Integer, y As Integer, x As Integer) As Boolean
        CheckBandC = False
        If y < 0 OrElse x + 1 < 0 OrElse y >= H OrElse x + 2 >= W Then Exit Function
        If tmp(y, x + 1) >= Knight Then Exit Function
        If tmp(y, x + 2) >= Knight Then Exit Function
        If IsNoEmpty(tmp, y, x - 1) Then Exit Function
        If IsNoEmpty(tmp, y, x    ) Then Exit Function
        If IsNoEmpty(tmp, y, x + 3) Then Exit Function
        If IsNoEmpty(tmp, y, x + 4) Then Exit Function
        CheckBandC = True
    End Function

    Private Function FillC(bd(,) As Integer, up As Integer) As FillRet
        Dim tmp(,) As Integer = CType(bd.Clone(), Integer(,))
        Dim cnt As Integer = 0
        Dim flag(W - 1) As Boolean
        Do
            Dim changes = 0
            Dim x As Integer = 0
            Do While x < W - 3
                If flag(x) Then
                    x += 1
                    Continue Do
                End If
                flag(x) = True
                Dim y As Integer = 0
                Dim p As Integer = 0
                Do While y < H - 3
                    If Not CheckFirstC(tmp, y, x) Then
                        y += 1
                        Continue Do
                    End If
                    Dim y0 As Integer = y
                    y += 1
                    If Not CheckBandC(tmp, y, x) Then
                        y += 1
                        Continue Do
                    End If
                    Dim y1 As Integer = -1
                    y += 1
                    Do While y < H - 1
                        If Not CheckBandC(tmp, y, x) Then
                            Exit Do
                        End If
                        y += 1
                        If Not CheckLastC(tmp, y, x) Then
                            Continue Do
                        End If
                        y1 = y
                    Loop
                    If y1 < 0 Then Continue Do
                    cnt += PutKnight(tmp, y0, x)
                    cnt += PutKnight(tmp, y0, x + 3)
                    cnt += PutKnight(tmp, y1, x)
                    cnt += PutKnight(tmp, y1, x + 3)
                    For ty As Integer = y0 + 1 To y1 - 1
                        cnt += PutKnight(tmp, ty, x + 1)
                        cnt += PutKnight(tmp, ty, x + 2)
                    Next ty
                    changes += 1
                    p += 1
                Loop
                If p > 0 Then x += up
            Loop
            If changes = 0 Then Exit Do
        Loop
        FillC = New FillRet(cnt, tmp)
    End Function

    Private Function FillD(bd(,) As Integer, down As Integer) As FillRet
        Dim tmp(,) As Integer = CType(bd.Clone(), Integer(,))
        Dim cnt As Integer = 0
        Dim flag(W - 1) As Boolean
        Do
            Dim changes = 0
            Dim x As Integer = W - 4
            Do While x >= 0
                If flag(x) Then
                    x -= 1
                    Continue Do
                End If
                flag(x) = True
                Dim y As Integer = 0
                Dim p As Integer = 0
                Do While y < H - 3
                    If Not CheckFirstC(tmp, y, x) Then
                        y += 1
                        Continue Do
                    End If
                    Dim y0 As Integer = y
                    y += 1
                    If Not CheckBandC(tmp, y, x) Then
                        y += 1
                        Continue Do
                    End If
                    Dim y1 As Integer = -1
                    y += 1
                    Do While y < H - 1
                        If Not CheckBandC(tmp, y, x) Then
                            Exit Do
                        End If
                        y += 1
                        If Not CheckLastC(tmp, y, x) Then
                            Continue Do
                        End If
                        y1 = y
                    Loop
                    If y1 < 0 Then Continue Do
                    cnt += PutKnight(tmp, y0, x)
                    cnt += PutKnight(tmp, y0, x + 3)
                    cnt += PutKnight(tmp, y1, x)
                    cnt += PutKnight(tmp, y1, x + 3)
                    For ty As Integer = y0 + 1 To y1 - 1
                        cnt += PutKnight(tmp, ty, x + 1)
                        cnt += PutKnight(tmp, ty, x + 2)
                    Next ty
                    changes += 1
                    p += 1
                Loop
                If p > 0 Then x -= down
            Loop
            If changes = 0 Then Exit Do
        Loop
        FillD = New FillRet(cnt, tmp)
    End Function

    Const DfsLimitOld As Integer = 10000

    Private Function DfsOld(tmp(,) As Integer, y As Integer, x As Integer, ByRef count As Integer) As Integer
        DfsOld = 0
        count += 1
        If count > DfsLimitOld Then Exit Function
        PutKnight(tmp, y, x)
        For d As Integer = 0 To UBound(dxs)
            Dim ty As Integer = y + dys(d)
            If ty < 0 OrElse ty >= H Then Continue For
            Dim tx As Integer = x + dxs(d)
            If tx < 0 OrElse tx >= W Then Continue For
            Dim p As Integer = tmp(ty, tx) - KeepOut
            If p = 0 Then
                Dim cnt As Integer = DfsOld(tmp, ty, tx, count)
                If cnt > 0 Then
                    DfsOld = cnt + 1
                    Exit Function
                End If
            ElseIf p = 1 Then
                PutKnight(tmp, ty, tx)
                DfsOld = 2
                Exit Function
            End If
        Next d

        RemoveKnight(tmp, y, x)
    End Function

    Private Function ConnectOld(tmp(,) As Integer) As Integer
        Dim cnt As Integer = 0
        For y As Integer = 0 To H - 1
            For x As Integer = 0 To W - 1
                If tmp(y, x) <> 1 Then Continue For
                cnt += DfsOld(tmp, y, x, 0)
            Next x
        Next y
        ConnectOld = cnt
    End Function

    Private Function KnightLoopOld(tmp(,) As Integer) As Integer
        Dim cnt As Integer = 0
        For y As Integer = 0 To H - 1
            For x As Integer = 0 To W - 1
                If tmp(y, x) = 1 Then
                    tmp(y, x) = tmp(y, x) Or Mask
                End If
            Next x
        Next y
        For y As Integer = 0 To H - 1
            For x As Integer = 0 To W - 1
                If tmp(y, x) <> 0 Then Continue For
                Dim c As Boolean = True
                For d As Integer = 0 To UBound(dxs)
                    Dim ty As Integer = y + dys(d)
                    If ty < 0 OrElse ty >= H Then Continue For
                    Dim tx As Integer = x + dxs(d)
                    If tx < 0 OrElse tx >= W Then Continue For
                    If tmp(ty, tx) <> 0 Then Continue For
                    tmp(ty, tx) = 1
                    c = False
                Next d
                If c Then Continue For
                tmp(y, x) = tmp(y, x) Or Knight
                For d As Integer = 0 To UBound(dxs)
                    Dim ty As Integer = y + dys(d)
                    If ty < 0 OrElse ty >= H Then Continue For
                    Dim tx As Integer = x + dxs(d)
                    If tx < 0 OrElse tx >= W Then Continue For
                    If tmp(ty, tx) <> 1 Then Continue For
                    Dim ret = DfsOld(tmp, ty, tx, 0)
                    If ret > 0 Then
                        cnt += ret
                        c = True
                        Exit For
                    End If
                Next d
                For d As Integer = 0 To UBound(dxs)
                    Dim ty As Integer = y + dys(d)
                    If ty < 0 OrElse ty >= H Then Continue For
                    Dim tx As Integer = x + dxs(d)
                    If tx < 0 OrElse tx >= W Then Continue For
                    If tmp(ty, tx) <> 1 Then Continue For
                    tmp(ty, tx) = 0
                Next d
                tmp(y, x) = tmp(y, x) Xor Knight
                If c Then
                    cnt += PutKnight(tmp, y, x)
                End If
            Next x
        Next y
        ' For y As Integer = 0 To H - 1
            ' For x As Integer = 0 To W - 1
                ' If (tmp(y, x) And Mask) <> 0 Then
                    ' tmp(y, x) = tmp(y, x) Xor Mask
                ' End If
            ' Next x
        ' Next y
        KnightLoopOld = cnt
    End Function

    Private Function FillWIncDec(tmp(,) As Integer) As Integer
        Dim cnt As Integer = 0
        For y As Integer = 0 To H - 3
            For x As Integer = W - 5 To 0 Step -1
                If NoZeroAndNoPawn(tmp(y + 1, x    )) Then Continue For
                If NoZeroAndNoPawn(tmp(y    , x + 2)) Then Continue For
                If NoZeroAndNoPawn(tmp(y + 2, x + 2)) Then Continue For
                If NoZeroAndNoPawn(tmp(y + 1, x + 4)) Then Continue For
                cnt += PutKnight(tmp, y + 1, x    )
                cnt += PutKnight(tmp, y    , x + 2)
                cnt += PutKnight(tmp, y + 2, x + 2)
                cnt += PutKnight(tmp, y + 1, x + 4)
            Next x
        Next y
        FillWIncDec = cnt
    End Function

    Private Function FillHIncDec(tmp(,) As Integer) As Integer
        Dim cnt As Integer = 0
        For x As Integer = 0 To W - 3
            For y As Integer = H - 5 To 0 Step -1
                If NoZeroAndNoPawn(tmp(y    , x + 1)) Then Continue For
                If NoZeroAndNoPawn(tmp(y + 2, x    )) Then Continue For
                If NoZeroAndNoPawn(tmp(y + 2, x + 2)) Then Continue For
                If NoZeroAndNoPawn(tmp(y + 4, x + 1)) Then Continue For
                cnt += PutKnight(tmp, y    , x + 1)
                cnt += PutKnight(tmp, y + 2, x    )
                cnt += PutKnight(tmp, y + 2, x + 2)
                cnt += PutKnight(tmp, y + 4, x + 1)
            Next y
        Next x
        FillHIncDec = cnt
    End Function

    Private Function FillWDecDec(tmp(,) As Integer) As Integer
        Dim cnt As Integer = 0
        For y As Integer = H - 3 To 0 Step -1
            For x As Integer = W - 5 To 0 Step -1
                If NoZeroAndNoPawn(tmp(y + 1, x    )) Then Continue For
                If NoZeroAndNoPawn(tmp(y    , x + 2)) Then Continue For
                If NoZeroAndNoPawn(tmp(y + 2, x + 2)) Then Continue For
                If NoZeroAndNoPawn(tmp(y + 1, x + 4)) Then Continue For
                cnt += PutKnight(tmp, y + 1, x    )
                cnt += PutKnight(tmp, y    , x + 2)
                cnt += PutKnight(tmp, y + 2, x + 2)
                cnt += PutKnight(tmp, y + 1, x + 4)
            Next x
        Next y
        FillWDecDec = cnt
    End Function

    Private Function FillHDecDec(tmp(,) As Integer) As Integer
        Dim cnt As Integer = 0
        For x As Integer = W - 3 To 0 Step -1
            For y As Integer = H - 5 To 0 Step -1
                If NoZeroAndNoPawn(tmp(y    , x + 1)) Then Continue For
                If NoZeroAndNoPawn(tmp(y + 2, x    )) Then Continue For
                If NoZeroAndNoPawn(tmp(y + 2, x + 2)) Then Continue For
                If NoZeroAndNoPawn(tmp(y + 4, x + 1)) Then Continue For
                cnt += PutKnight(tmp, y    , x + 1)
                cnt += PutKnight(tmp, y + 2, x    )
                cnt += PutKnight(tmp, y + 2, x + 2)
                cnt += PutKnight(tmp, y + 4, x + 1)
            Next y
        Next x
        FillHDecDec = cnt
    End Function

    Private Function FillWDecInc(tmp(,) As Integer) As Integer
        Dim cnt As Integer = 0
        For y As Integer = H - 3 To 0 Step -1
            For x As Integer = 0 To W - 5
                If NoZeroAndNoPawn(tmp(y + 1, x    )) Then Continue For
                If NoZeroAndNoPawn(tmp(y    , x + 2)) Then Continue For
                If NoZeroAndNoPawn(tmp(y + 2, x + 2)) Then Continue For
                If NoZeroAndNoPawn(tmp(y + 1, x + 4)) Then Continue For
                cnt += PutKnight(tmp, y + 1, x    )
                cnt += PutKnight(tmp, y    , x + 2)
                cnt += PutKnight(tmp, y + 2, x + 2)
                cnt += PutKnight(tmp, y + 1, x + 4)
            Next x
        Next y
        FillWDecInc = cnt
    End Function

    Private Function FillHDecInc(tmp(,) As Integer) As Integer
        Dim cnt As Integer = 0
        For x As Integer = W - 3 To 0 Step -1
            For y As Integer = 0 To H - 5
                If NoZeroAndNoPawn(tmp(y    , x + 1)) Then Continue For
                If NoZeroAndNoPawn(tmp(y + 2, x    )) Then Continue For
                If NoZeroAndNoPawn(tmp(y + 2, x + 2)) Then Continue For
                If NoZeroAndNoPawn(tmp(y + 4, x + 1)) Then Continue For
                cnt += PutKnight(tmp, y    , x + 1)
                cnt += PutKnight(tmp, y + 2, x    )
                cnt += PutKnight(tmp, y + 2, x + 2)
                cnt += PutKnight(tmp, y + 4, x + 1)
            Next y
        Next x
        FillHDecInc = cnt
    End Function

    Private Function ApplyRand(tmp(,) As Integer) As Integer
        Dim cnt As Integer = 0
        Dim ps() As Integer = dfsList(rand.Next(dfsList.Count))
        For Each p As Integer In ps
            Dim ty, tx As Integer
            I2P(p, ty, tx)
            cnt += PutKnight(tmp, ty, tx)
        Next p
        ApplyRand = cnt
    End Function

    Private Function ConnectRand(tmp(,) As Integer) As Integer
        Dim cnt As Integer = 0
        For y As Integer = 0 To H - 1
            For x As Integer = 0 To W - 1
                If tmp(y, x) <> 1 Then Continue For
                If DfsEx(tmp, y, x, 0) = 0 Then Continue For
                cnt += ApplyRand(tmp)
            Next x
        Next y
        ConnectRand = cnt
    End Function

    Private Function KnightLoopRand(tmp(,) As Integer) As Integer
        Dim cnt As Integer = 0
        For y As Integer = 0 To H - 1
            For x As Integer = 0 To W - 1
                If tmp(y, x) = 1 Then
                    tmp(y, x) = tmp(y, x) Or Mask
                End If
            Next x
        Next y
        For y As Integer = 0 To H - 1
            For x As Integer = 0 To W - 1
                If tmp(y, x) <> 0 Then Continue For
                Dim c As Boolean = True
                For d As Integer = 0 To UBound(dxs)
                    Dim ty As Integer = y + dys(d)
                    If ty < 0 OrElse ty >= H Then Continue For
                    Dim tx As Integer = x + dxs(d)
                    If tx < 0 OrElse tx >= W Then Continue For
                    If tmp(ty, tx) <> 0 Then Continue For
                    tmp(ty, tx) = 1
                    c = False
                Next d
                If c Then Continue For
                tmp(y, x) = tmp(y, x) Or Knight
                For d As Integer = 0 To UBound(dxs)
                    Dim ty As Integer = y + dys(d)
                    If ty < 0 OrElse ty >= H Then Continue For
                    Dim tx As Integer = x + dxs(d)
                    If tx < 0 OrElse tx >= W Then Continue For
                    If tmp(ty, tx) <> 1 Then Continue For
                    Dim ret = DfsEx(tmp, ty, tx, 0)
                    If ret > 0 Then
                        cnt += ApplyRand(tmp)
                        c = True
                        Exit For
                    End If
                Next d
                For d As Integer = 0 To UBound(dxs)
                    Dim ty As Integer = y + dys(d)
                    If ty < 0 OrElse ty >= H Then Continue For
                    Dim tx As Integer = x + dxs(d)
                    If tx < 0 OrElse tx >= W Then Continue For
                    If tmp(ty, tx) <> 1 Then Continue For
                    tmp(ty, tx) = 0
                Next d
                tmp(y, x) = tmp(y, x) Xor Knight
                If c Then
                    cnt += PutKnight(tmp, y, x)
                End If
            Next x
        Next y
        ' For y As Integer = 0 To H - 1
            ' For x As Integer = 0 To W - 1
                ' If (tmp(y, x) And Mask) <> 0 Then
                    ' tmp(y, x) = tmp(y, x) Xor Mask
                ' End If
            ' Next x
        ' Next y
        KnightLoopRand = cnt
    End Function

    Private Function FillRemsRand(ByRef tmp(,) As Integer) As Integer
        Dim cnt As Integer = 0
        Dim emp As New Queue(Of Tp)()
        Dim flag(H - 1, W - 1) As Tp
        For y As Integer = 0 To H - 1
            For x As Integer = 0 To W - 1
                If tmp(y, x) <> 2 Then Continue For
                Dim t As New Tp(y, x)
                emp.Enqueue(t)
                flag(y, x) = t
            Next x
        Next y
        Dim ec As Integer = emp.Count
        For i As Integer = 1 To ec
            Dim t As Tp = emp.Dequeue()
            If tmp(t.Item1, t.Item2) = 2 Then
                cnt += PutKnight(tmp, t.Item1, t.Item2)
            Else
                emp.Enqueue(t)
            End If
        Next i
        If emp.Count = 0 Then
            FillRemsRand = cnt
            Exit Function
        End If
        ' Console.Error.WriteLine("ec {0}", ec)
        Dim tmpZ(,) As Integer = CType(tmp.Clone(), Integer(,))
        Dim cntZ As Integer = cnt
        Dim shf As Integer = Math.Max(2, emp.Count \ 2)
        For i As Integer = 1 To ec * 5
            If emp.Count = 0 Then Exit For
            If i Mod shf = 0 Then
                Dim qs() As Tp = emp.ToArray()
                Shuffle(qs)
                emp.Clear()
                For Each q As Tp In qs
                    emp.Enqueue(q)
                Next q
            End If
            Dim t As Tp = emp.Dequeue()
            Dim y As Integer = t.Item1
            Dim x As Integer = t.Item2
            If tmpZ(y, x) = 2 Then
                cntZ += PutKnight(tmpZ, y, x)
                If cntZ > cnt Then
                    cnt = cntZ
                    tmp = CType(tmpZ.Clone(), Integer(,))
                    ' CopyArray2D(tmpZ, tmp)
                    ' Console.Error.WriteLine("upd1")
                End If
                Continue For
            End If
            Dim nc As Boolean = True
            For d As Integer = 0 To UBound(dxs)
                Dim ty As Integer = y + dys(d)
                If ty < 0 OrElse ty >= H Then Continue For
                Dim tx As Integer = x + dxs(d)
                If tx < 0 OrElse tx >= W Then Continue For
                If flag(ty, tx) Is Nothing Then Continue For
                If (tmpZ(ty, tx) And Knight) = 0 Then Continue For
                nc = False
                cntZ -= RemoveKnight(tmpZ, ty, tx)
                emp.Enqueue(flag(ty, tx))
            Next d
            If nc Then Continue For
            If tmpZ(y, x) <> 2 Then Continue For
            cntZ += PutKnight(tmpZ, y, x)
            If cntZ > cnt Then
                cnt = cntZ
                tmp = CType(tmpZ.Clone(), Integer(,))
                ' CopyArray2D(tmpZ, tmp)
                ' Console.Error.WriteLine("upd2")
            End If
        Next i
        FillRemsRand = cnt
    End Function

    Private Sub Shuffle(Of T)(vs() As T)
        For i As Integer = 0 To UBound(vs)
            Dim j As Integer = rand.Next(vs.Length)
            Dim v As T = vs(i)
            vs(i) = vs(j)
            vs(j) = v
        Next i
    End Sub

End Class