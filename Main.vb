Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console

Public Module Main

	Public Sub Main()
		Try
			Dim H As Integer = CInt(Console.ReadLine())
			Dim board(H - 1) As String
			For i As Integer = 0 To UBound(board)
				board(i) = Console.ReadLine()
			Next i
			
			Dim kap As New KnightsAndPawns()
			Dim ret() As String = kap.placeKnights(board)

			Console.WriteLine(ret.Length)
			For Each r As String In ret
				Console.WriteLine(r)
			Next r
			Console.Out.Flush()
			
		Catch Ex As Exception
			Console.Error.WriteLine(ex)
		End Try
	End Sub

End Module